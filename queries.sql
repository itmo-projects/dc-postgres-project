-- SET search_path TO carsharing_operations;

-- Клиенты с наиболее высоким накопленным кэшбэком
SELECT c.idClient, c.name, c.surname, c.current_cashback
FROM client c
ORDER BY c.current_cashback DESC
LIMIT 10;

-- Поездки на определенный день
SELECT * FROM ride
WHERE DATE(start_time) = '2024-03-15'
ORDER BY idride ASC;

-- Поездки за период времени c длительностью поездки
SELECT r.idride,
  c.email AS client_email,
  EXTRACT(EPOCH FROM (r.end_time - r.start_time)) / 60 AS ride_duration_minutes ,
        v.plate_number AS Vehicle_Plate
FROM ride r
JOIN booking b ON r.booking_id = b.idbooking
JOIN client c ON r.user_id = c.idClient
JOIN (
    SELECT v.idVehicle, v.plate_number
    FROM vehicle v
    -- WHERE v.plate_number = 'НВЦ644ЮВ' -- зависит от сгенерированных данных
) v ON r.vehicle_id = v.idVehicle
WHERE DATE(r.start_time) BETWEEN '2024-03-15' AND '2024-03-17' AND r.status = 'FINISHED';

-- Аггрегационная выборка по всем автомобилям 
-- (выводится гос номер, количество поездок завершенных за период и среднее время поездки)
SELECT v.plate_number AS vehicle_plate_number,
       COUNT(r.idride) AS number_of_rides,
       ROUND(AVG(EXTRACT(EPOCH FROM (r.end_time - r.start_time)) / 60)) AS avg_ride_duration_minutes
FROM ride r
JOIN vehicle v ON r.vehicle_id = v.idVehicle
JOIN booking b ON r.booking_id = b.idbooking
JOIN client c ON r.user_id = c.idClient
WHERE DATE(r.start_time) BETWEEN '2024-03-15' AND '2024-03-17' AND r.status = 'FINISHED'
GROUP BY v.plate_number;



-- Машины в окружности ~1км Корпуса ИТМО на Ломоносова
select * from vehicle where current_location <@ circle(point '(59.926679, 30.339696)', 0.01);

-- Машины в опеределенным квадрате Санкт-Петербурга
SELECT * FROM vehicle WHERE current_location <@ BOX '((59.8,30.2), (60.2,30.4))';
