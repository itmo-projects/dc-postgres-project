# ITMO Digital culture data analysis diploma 
Тип проекта: Разработка БД (Postgres)
## Структура проекта
`init.sql` - Скрипт создание базы данных, создание схемы данных, создание таблиц и последовательностей </br>
`create_data.sql` - Создание процедур для генерации данных, а так же их вызов </br>
`queries.sql` - Примеры типовых запросов созданные в базе данных </br>

![alt text](ERD.png "ER Diagram")