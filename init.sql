-- Create the 'carsharing' database
-- CREATE DATABASE carsharing;

-- Connect to the 'carsharing' database
-- \c carsharing;

-- Create the schema 'carsharing_operations'
-- CREATE SCHEMA carsharing_operations;

-- Connect to the 'carsharing_operations' schema
-- SET search_path TO carsharing_operations;

-- Create the 'loyalty_plan' table
CREATE TABLE loyalty_plan (
    idloyalty_plan INT PRIMARY KEY,
    plan_title VARCHAR,
    cashback_rate NUMERIC CHECK (cashback_rate >= 0 AND cashback_rate <= 100),
    updated_at TIMESTAMP
);

-- Add comments to the 'loyalty_plan' table
COMMENT ON COLUMN loyalty_plan.idloyalty_plan IS 'Unique identifier for the loyalty plan';
COMMENT ON COLUMN loyalty_plan.plan_title IS 'Title of the loyalty plan';
COMMENT ON COLUMN loyalty_plan.cashback_rate IS 'Percentage rate of cashback provided by the plan';
COMMENT ON COLUMN loyalty_plan.updated_at IS 'Timestamp of the last update to the plan';

CREATE TABLE user_doc (
    iduser_doc INT PRIMARY KEY,
    doctype VARCHAR(20) CHECK (doctype IN ('PASSPORT_RU', 'PASSPORT_FOREIGNER')),
    docnumber VARCHAR(10),
    issuredate DATE
);

-- Add comments to the 'user_doc' table
COMMENT ON COLUMN user_doc.iduser_doc IS 'Unique identifier for the user document';
COMMENT ON COLUMN user_doc.doctype IS 'Type of the document';
COMMENT ON COLUMN user_doc.docnumber IS 'Document number';
COMMENT ON COLUMN user_doc.issuredate IS 'Date when the document was issued';

CREATE TABLE driving_license (
    iddriving_license INT PRIMARY KEY,
    issued_at DATE NOT NULL,
    category CHAR(1) CHECK (category IN ('A', 'B', 'C', 'D')),
    experience INT CHECK (experience >= 0 AND experience <= 99),
    number VARCHAR(20)
);

-- Add comments to the 'driving_license' table
COMMENT ON COLUMN driving_license.iddriving_license IS 'Unique identifier for the driving license';
COMMENT ON COLUMN driving_license.issued_at IS 'Date when the license was issued';
COMMENT ON COLUMN driving_license.category IS 'Category of the driving license (A, B, C, D)';
COMMENT ON COLUMN driving_license.experience IS 'Number of years of driving experience';
COMMENT ON COLUMN driving_license.number IS 'License number';

CREATE TABLE registration_doc (
    idregistration_doc INT PRIMARY KEY,
    docnumber VARCHAR(20),
    issued_at DATE,
    VIN VARCHAR(45),
    owner VARCHAR(45),
    vehicle_model VARCHAR(45)
);

-- Add comments to the 'registration_doc' table
COMMENT ON COLUMN registration_doc.idregistration_doc IS 'Unique identifier for the vehicle registration document';
COMMENT ON COLUMN registration_doc.docnumber IS 'Document number';
COMMENT ON COLUMN registration_doc.issued_at IS 'Date when the document was issued';
COMMENT ON COLUMN registration_doc.VIN IS 'Vehicle Identification Number';
COMMENT ON COLUMN registration_doc.owner IS 'Name of the vehicle owner';
COMMENT ON COLUMN registration_doc.vehicle_model IS 'Model of the vehicle';

CREATE TABLE vehicle (
    idVehicle INT PRIMARY KEY,
    fuel_rate NUMERIC CHECK (fuel_rate >= 0 AND fuel_rate <= 100),
    current_location POINT, -- Assuming you're using PostgreSQL's geometric types for location
    plate_number VARCHAR(45),
    vehicle_model VARCHAR(45),
    registration_doc_id INT, -- Foreign key referencing registration_doc
    FOREIGN KEY (registration_doc_id) REFERENCES registration_doc(idregistration_doc)
);

-- Add comments to the 'vehicle' table
COMMENT ON COLUMN vehicle.idVehicle IS 'Unique identifier for the vehicle';
COMMENT ON COLUMN vehicle.fuel_rate IS 'Fuel efficiency rating of the vehicle';
COMMENT ON COLUMN vehicle.current_location IS 'Current location of the vehicle';
COMMENT ON COLUMN vehicle.plate_number IS 'License plate number of the vehicle';
COMMENT ON COLUMN vehicle.vehicle_model IS 'Model of the vehicle';
COMMENT ON COLUMN vehicle.registration_doc_id IS 'Foreign key to the vehicle registration document';

CREATE TABLE client (
    idClient INT PRIMARY KEY,
    phone_number VARCHAR(45),
    email VARCHAR(255),
    name VARCHAR(100),
    surname VARCHAR(100),
    last_name VARCHAR(100),
    status VARCHAR(20) CHECK (status IN ('ACTIVE', 'BLOCKED', 'DELETED')),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    current_cashback NUMERIC,
    user_doc_id INT, -- Foreign key referencing user_doc
    loyalty_plan_id INT, -- Foreign key referencing loyalty_plan
    driving_license_id INT, -- Foreign key referencing driving_license

    FOREIGN KEY (user_doc_id) REFERENCES user_doc(iduser_doc),
FOREIGN KEY (loyalty_plan_id) REFERENCES loyalty_plan(idloyalty_plan),
FOREIGN KEY (driving_license_id) REFERENCES driving_license(iddriving_license)

);

-- Add comments to the 'client' table
COMMENT ON COLUMN client.idClient IS 'Unique identifier for the client';
COMMENT ON COLUMN client.phone_number IS 'Client phone number';
COMMENT ON COLUMN client.email IS 'Client email address';
COMMENT ON COLUMN client.name IS 'Client first name';
COMMENT ON COLUMN client.surname IS 'Client surname';
COMMENT ON COLUMN client.last_name IS 'Client last name';
COMMENT ON COLUMN client.status IS 'Status of the client (active, blocked, deleted)';
COMMENT ON COLUMN client.created_at IS 'Timestamp when the client was created';
COMMENT ON COLUMN client.updated_at IS 'Timestamp of the last update to the client information';
COMMENT ON COLUMN client.current_cashback IS 'Current cashback balance of the client';
COMMENT ON COLUMN client.user_doc_id IS 'Foreign key to the client document';
COMMENT ON COLUMN client.loyalty_plan_id IS 'Foreign key to the client loyalty plan';
COMMENT ON COLUMN client.driving_license_id IS 'Foreign key to the client driving license';

CREATE TABLE booking (
    idbooking INT PRIMARY KEY,
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    status VARCHAR(20) CHECK (status IN ('BOOKED', 'CANCELLED', 'IN_RIDE', 'RIDE_FINISHED')),
    user_id INT, -- Foreign key referencing user
    FOREIGN KEY (user_id) REFERENCES client(idClient),
    vehicle_id INT, -- Foreign key referencing user
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(idVehicle)
);

-- Add comments to the 'booking' table
COMMENT ON COLUMN booking.idbooking IS 'Unique identifier for the booking';
COMMENT ON COLUMN booking.start_time IS 'Start time of the booking';
COMMENT ON COLUMN booking.end_time IS 'End time of the booking';
COMMENT ON COLUMN booking.status IS 'Status of the booking (booked, cancelled, in ride, ride finished)';
COMMENT ON COLUMN booking.user_id IS 'Foreign key to the client who made the booking';

CREATE TABLE ride (
    idRide INT PRIMARY KEY,
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    start_location POINT,
    end_location POINT,
    status VARCHAR(20) CHECK (status IN ('ACTIVE', 'FINISHED', 'PROBLEM')),
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES client(idClient),
    booking_id INT,
    FOREIGN KEY (booking_id) REFERENCES booking(idbooking),
    vehicle_id INT,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(idVehicle)
);

-- Add comments to the 'ride' table
COMMENT ON COLUMN ride.idRide IS 'Unique identifier for the ride';
COMMENT ON COLUMN ride.start_time IS 'Start time of the ride';
COMMENT ON COLUMN ride.end_time IS 'End time of the ride';
COMMENT ON COLUMN ride.start_location IS 'Starting location of the ride (geometric point)';
COMMENT ON COLUMN ride.end_location IS 'Ending location of the ride (geometric point)';
COMMENT ON COLUMN ride.status IS 'Status of the ride (active, finished, problem)';
COMMENT ON COLUMN ride.user_id IS 'Foreign key to the client who took the ride';
COMMENT ON COLUMN ride.booking_id IS 'Foreign key to the booking associated with the ride';

CREATE TABLE price_type (
    idprice_type INT PRIMARY KEY,
    price_type VARCHAR(10) NOT NULL,
    price_info VARCHAR(255)
);

-- Add comments to the 'price_type' table
COMMENT ON COLUMN price_type.idprice_type IS 'Unique identifier for the price type';
COMMENT ON COLUMN price_type.price_type IS 'Type of the price (e.g., per minute, per mile)';
COMMENT ON COLUMN price_type.price_info IS 'Description of the price type';


CREATE TABLE price (
    idprice INT PRIMARY KEY,
    price DECIMAL(10, 2) NOT NULL,
    idprice_type INT,
    FOREIGN KEY (idprice_type) REFERENCES price_type(idprice_type)
    -- idVehicle INT,
    -- FOREIGN KEY (idVehicle) REFERENCES vehicle(idVehicle)
);

-- Add comments to the 'price' table
COMMENT ON COLUMN price.idprice IS 'Unique identifier for the price';
COMMENT ON COLUMN price.price IS 'Amount of the price';
COMMENT ON COLUMN price.idprice_type IS 'Foreign key to the price type';
-- COMMENT ON COLUMN price.idVehicle IS 'Foreign key to the vehicle associated with the price';


CREATE TABLE bill (
    idbill INT PRIMARY KEY,
    status VARCHAR(40) CHECK (status IN ('PAID', 'PAYMENT_REQUIRED')),
    amount DECIMAL(10, 2) NOT NULL,
    cashback DECIMAL(5, 2),
    idprice INT,
    idprice_type INT,
    FOREIGN KEY (idprice) REFERENCES price(idprice),
    FOREIGN KEY (idprice_type) REFERENCES price_type(idprice_type),
    idride INT,
    FOREIGN KEY (idride) REFERENCES ride(idRide)
);

-- Add comments to the 'bill' table
COMMENT ON COLUMN bill.idbill IS 'Unique identifier for the bill';
COMMENT ON COLUMN bill.status IS 'Status of the bill (paid, payment required)';
COMMENT ON COLUMN bill.amount IS 'Amount of the bill';


CREATE SEQUENCE price_id_seq;
CREATE SEQUENCE booking_id_seq;
CREATE SEQUENCE ride_id_seq;
CREATE SEQUENCE bill_id_seq;
