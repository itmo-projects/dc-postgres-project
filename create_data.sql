-- SET search_path TO carsharing_operations;

CREATE OR REPLACE PROCEDURE generate_data_part_1()
LANGUAGE plpgsql
AS $$
DECLARE
    i INT;
BEGIN
    -- Создание 5 строк в таблицу loyalty_plan
    FOR i IN 1..5 LOOP
        INSERT INTO loyalty_plan (idloyalty_plan, plan_title, cashback_rate, updated_at)
        VALUES (i,
                CASE i
                    WHEN 1 THEN 'BEGINNER'
                    WHEN 2 THEN 'MOTORIST'
                    WHEN 3 THEN 'ENTHUSIAST'
                    WHEN 4 THEN 'MACHINE'
                    WHEN 5 THEN 'ROAD_STAR'
                END,
                i * 5,
                CURRENT_DATE);
    END LOOP;

    -- Создание 1000 строк в таблицу driving_license
    FOR i IN 1..1000 LOOP
        INSERT INTO driving_license (idDriving_license, issued_at, category, experience, number)
        VALUES (i,
                CURRENT_DATE - INTERVAL '3 years' + (RANDOM() * INTERVAL '3 years'),
                'B',
                3 + (RANDOM() * 12),
                LPAD((RANDOM() * 999999999999999)::BIGINT::TEXT, 15, '0'));
    END LOOP;

    -- Создание 1000 строк в таблицу user_doc
    FOR i IN 1..1000 LOOP
        INSERT INTO user_doc (iduser_doc, doctype, docnumber, issuredate)
        VALUES (i,
                CASE WHEN RANDOM() < 0.5 THEN 'PASSPORT_RU' ELSE 'PASSPORT_FOREIGNER' END,
                LPAD((RANDOM() * 9999999999)::BIGINT::TEXT, 10, '0'),
                TIMESTAMP '2018-01-01' + (RANDOM() * (TIMESTAMP '2024-01-01' - TIMESTAMP '2018-01-01')));
    END LOOP;

    -- Создание 1000 строк в таблице client
    FOR i IN 1..1000 LOOP
        INSERT INTO client (idClient, phone_number, email, name, surname, last_name, status, current_cashback, user_doc_id, loyalty_plan_id, driving_license_id)
        VALUES (i,
                '+7'||LPAD((RANDOM() * 9999999999)::BIGINT::TEXT, 10, '0'),
                'user'||i||'@sber.ru',
                CASE WHEN RANDOM() < 0.5 THEN 'Иван' ELSE 'Александр' END,
                CASE WHEN RANDOM() < 0.5 THEN 'Петров' ELSE 'Иванов' END,
                CASE WHEN RANDOM() < 0.5 THEN 'Сергеевич' ELSE 'Александрович' END,
                CASE WHEN i <= 50 THEN 'BLOCKED' WHEN i > 950 THEN 'DELETED' ELSE 'ACTIVE' END,
                RANDOM() * 678,
                (SELECT iduser_doc FROM user_doc ORDER BY RANDOM() LIMIT 1),
                (SELECT idloyalty_plan FROM loyalty_plan ORDER BY RANDOM() LIMIT 1),
                (SELECT iddriving_license FROM driving_license ORDER BY RANDOM() LIMIT 1));
    END LOOP;

    -- Создание 100 строк в таблицу registration_doc
FOR i IN 1..100 LOOP
    INSERT INTO registration_doc (idregistration_doc, docnumber, issued_at, VIN, owner, vehicle_model)
    VALUES (i,
            substring(md5(random()::text), 1, 20),
            (SELECT max(start_date) + '1 day'::interval
             FROM generate_series(DATE '2020-01-01', DATE '2024-01-01', '1 day') AS start_date),
            substring(md5(random()::text), 1, 20),
            'OOO "Каршеринг"',
            CASE 
                WHEN random() < 0.33 THEN 'BUSINESS'
                WHEN random() >= 0.33 AND random() < 0.66 THEN 'PREMIUM'
                ELSE 'ECONOMY'
            END);
END LOOP;



    -- Создание 100 строк в таблицу vehicle
FOR i IN 1..100 LOOP
    INSERT INTO vehicle (idVehicle, fuel_rate, current_location, plate_number, vehicle_model, registration_doc_id)
    VALUES (
        i,
        floor(random() * 99) + 1,
        POINT(
            (59.8 + random() * 0.2)::numeric(8, 6), -- Latitude between 59.8 and 60.0
            (30.2 + random() * 0.3)::numeric(8, 6)  -- Longitude between 30.2 and 30.5
        ),
        chr(1040 + floor(random() * 32)::int) || -- First Russian letter
        chr(1040 + floor(random() * 32)::int) || -- Second Russian letter
        chr(1040 + floor(random() * 32)::int) || -- Third Russian letter
        floor(random() * 10)::text || -- First digit
        floor(random() * 10)::text || -- Second digit
        floor(random() * 10)::text || -- Third digit
        chr(1040 + floor(random() * 32)::int) || -- Fourth Russian letter
        chr(1040 + floor(random() * 32)::int), -- Fifth Russian letter
        CASE
            WHEN random() < 0.33 THEN 'BUSINESS'
            WHEN random() >= 0.33 AND random() < 0.66 THEN 'PREMIUM'
            ELSE 'ECONOMY'
        END,
        i
    );
END LOOP;




END;
$$;


CREATE OR REPLACE PROCEDURE generate_price_type_data()
LANGUAGE plpgsql
AS $$
DECLARE
    types text[] := ARRAY['min', 'hour', 'day', 'week', 'month'];
    info text[] := ARRAY['Стоимость аренды поминутно', 'Стоимость аренды в час', 'Стоимость аренды в день', 'Стоимость аренды в неделю', 'Стоимость аренды в месяц'];
    i INT;
BEGIN
    FOR i IN 1..5 LOOP
        INSERT INTO price_type (idprice_type, price_type, price_info)
        VALUES (i, types[i], info[i]);
    END LOOP;
END;
$$;


CREATE OR REPLACE PROCEDURE generate_price_data()
LANGUAGE plpgsql
AS $$
DECLARE
    type_ids INT[] := ARRAY(SELECT idprice_type FROM price_type);
    type_id INT;
    price DECIMAL;
    price_info VARCHAR(255);
    i INT;
    vehicle_id INT;
    price_id INT;
BEGIN
    FOR vehicle_id IN 1..100 LOOP
        FOR i IN 1..3 LOOP
            -- Choose a random type of price
            type_id := type_ids[1 + floor(random() * array_length(type_ids, 1))];
            
            -- Choose the price and price_info based on the type
            CASE type_id
                WHEN (SELECT idprice_type FROM price_type WHERE price_type = 'min') THEN
                    price := 7.0 + random() * (29.0 - 7.0);
                    price_info := 'Стоимость за минуту аренды';
                WHEN (SELECT idprice_type FROM price_type WHERE price_type = 'hour') THEN
                    price := 400 + random() * (1200 - 400);
                    price_info := 'Стоимость за час аренды';
                WHEN (SELECT idprice_type FROM price_type WHERE price_type = 'day') THEN
                    price := 300 + random() * (6500 - 300);
                    price_info := 'Стоимость за день аренды';
                WHEN (SELECT idprice_type FROM price_type WHERE price_type = 'week') THEN
                    price := 13000 + random() * (20000 - 13000);
                    price_info := 'Стоимость за неделю аренды';
                WHEN (SELECT idprice_type FROM price_type WHERE price_type = 'month') THEN
                    price := 20000 + random() * (100000 - 20000);
                    price_info := 'Стоимость за месяц аренды';
            END CASE;
            
            -- Insert the data into the price table
            -- Generate unique idprice using a sequence or some other mechanism
            SELECT nextval('price_id_seq') INTO price_id;
            INSERT INTO price (idprice, price, idprice_type)
            VALUES (price_id, price, type_id);
        END LOOP;
    END LOOP;
END;
$$;



CREATE OR REPLACE PROCEDURE generate_booking_data()
LANGUAGE plpgsql
AS $$
DECLARE
    user_ids INT[] := ARRAY(SELECT idClient FROM client);
    vehicle_ids INT[] := ARRAY(SELECT idVehicle FROM vehicle);
    user_id INT;
    i INT;
    start_time TIMESTAMP;
    end_time TIMESTAMP;
    booking_id INT;
BEGIN
    FOR user_id IN 1..1000 LOOP
        FOR i IN 1..3 LOOP
            -- Generate random start and end times within the specified range
            start_time := TIMESTAMP '2024-03-01' + (RANDOM() * INTERVAL '30 days');
            end_time := start_time + (RANDOM() * INTERVAL '20 minutes');
            
            -- Get the next value from the sequence for idbooking
            SELECT nextval('booking_id_seq') INTO booking_id;
            
            -- Insert the data into the booking table
            INSERT INTO booking (idbooking, start_time, end_time, status, user_id, vehicle_id)
            VALUES (booking_id, start_time, end_time, 'RIDE_FINISHED',
             user_ids[1 + floor(random() * array_length(user_ids, 1))],
             vehicle_ids[1 + floor(random() * array_length(vehicle_ids, 1))] 
             );
        END LOOP;
    END LOOP;
END;
$$;


CREATE OR REPLACE PROCEDURE generate_ride_data()
LANGUAGE plpgsql
AS $$
DECLARE
    booking_ids INT[] := ARRAY(SELECT idbooking FROM booking);
    user_ids INT[] := ARRAY(SELECT idClient FROM client);
    vehicle_ids INT[] := ARRAY(SELECT idVehicle FROM vehicle); -- Added
    i INT;
    start_time TIMESTAMP;
    end_time TIMESTAMP;
    duration INTERVAL;
    ride_id INT;
    start_lat FLOAT;
    start_lng FLOAT;
    end_lat FLOAT;
    end_lng FLOAT;
    vehicle_id INT; -- Declared vehicle_id variable
BEGIN
    FOREACH vehicle_id IN ARRAY vehicle_ids LOOP
        FOR i IN 1..250 LOOP
            -- Generate random start time within March 2024
            start_time := TIMESTAMP '2024-03-01' + (RANDOM() * INTERVAL '30 days');
            -- Generate random duration between 15 minutes and 2 hours
            duration := INTERVAL '15 minutes' + (RANDOM() * INTERVAL '105 minutes');
            -- Calculate end time
            end_time := start_time + duration;
            
            -- Generate random latitude and longitude for start location
            start_lat := (RANDOM() * 180) - 90;
            start_lng := (RANDOM() * 360) - 180;
            
            -- Generate random latitude and longitude for end location
            end_lat := (RANDOM() * 180) - 90;
            end_lng := (RANDOM() * 360) - 180;
            
            -- Get the next value from the sequence for idride
            SELECT nextval('ride_id_seq') INTO ride_id;
            
            -- Insert the data into the ride table
            INSERT INTO ride (idride, start_time, end_time, start_location, end_location, status, user_id, booking_id, vehicle_id) -- Modified
            VALUES (ride_id, start_time, end_time, POINT(start_lng, start_lat), POINT(end_lng, end_lat), 'FINISHED', user_ids[1 + floor(random() * array_length(user_ids, 1))], booking_ids[1 + floor(random() * array_length(booking_ids, 1))], vehicle_id);
        END LOOP;
    END LOOP;
END;
$$;





CREATE OR REPLACE PROCEDURE generate_bill_data()
LANGUAGE plpgsql
AS $$
DECLARE
    statuses VARCHAR[] := ARRAY['PAID', 'PAYMENT_REQUIRED'];
    status VARCHAR(40);
    amount DECIMAL;
    cashback DECIMAL;
    idprice INT;
    idprice_type_val INT; -- Distinct name for the variable
    ride_ids INT[] := ARRAY(SELECT idRide FROM ride);
    i INT;
    bill_id INT;
BEGIN
    FOR i IN 1..250 LOOP
        -- Select a random status
        status := statuses[1 + floor(random() * 2)];
        -- Generate a random amount between 100 and 4000
        amount := 100 + random() * 3900;
        -- Calculate cashback as 5% of the amount
        cashback := amount * 0.05;
        -- Select random idprice and idbooking
        idprice := 1 + floor(random() * 100);

        
        -- Select a random idprice_type from the price_type table
        SELECT idprice_type FROM price_type INTO idprice_type_val ORDER BY random() LIMIT 1;
        
        -- Get the next value from the sequence for idbill
        SELECT nextval('bill_id_seq') INTO bill_id;
        
        -- Insert the data into the bill table
        INSERT INTO bill (idbill, status, amount, cashback, idprice, idprice_type, idRide)
        VALUES (bill_id, status, amount, cashback, idprice, idprice_type_val,
        ride_ids[1 + floor(random() * array_length(ride_ids, 1))]);
    END LOOP;
END;
$$;





CALL generate_data_part_1();
CALL generate_price_type_data();
CALL generate_price_data();
CALL generate_booking_data();
CALL generate_ride_data();
CALL generate_bill_data();